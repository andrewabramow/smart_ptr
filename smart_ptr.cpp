/*
��� ����� �������:

���������� ����� ��������� shared_ptr_toy � ������������� ��������
� ��������� �������� ������ ��� ������ Toy, �������������� � �������.
��������� ������ ����� ��� ����������� ������ ������ � ����
�������������� ������� ������������� shared_ptr<Toy>.

���������� ��������� ������� make_shared_toy, ������� ���������
����� ���������� � ������������ ������� ���� �� ��������, ����
��� ������ ����� ������ �������.

���-���� ��� �������� ������

����� ���������� shared_ptr_toy
���������� �����������, ����������� �����, �������� ������������
������������, ���������� � ������� make_shared
*/

#include <iostream>

class Toy {
private:
	std::string name;
public:
	Toy(std::string _name) : name(_name) {};
	~Toy() {};
	const std::string get_name() const {
		return name;
	}
};

class ControlBlock {
private:
	int counter = 0;
public:
	ControlBlock(): counter (0) {
		std::cout << "Control block created\n";
	}
	const int get_counter() const {
		return counter;
	}
	void reset_counter(){
		counter = 0;
	}
	void increase_counter() {
		++counter;
	}
	void decrease_counter() {
		--counter;
	}
};

class shared_ptr_toy {
private:
	ControlBlock* cb;
	Toy* toy;
public:

	// Constructor
	shared_ptr_toy() : toy(nullptr), cb(nullptr) {}
	//  Copy asignment
	shared_ptr_toy& operator=(const shared_ptr_toy& oth_toy) {
		if (this == &oth_toy) return *this;
		if (toy != nullptr) delete toy;
		if (cb != nullptr) delete cb;
		toy = new Toy(*oth_toy.toy);
		cb = new ControlBlock(*oth_toy.cb);
		return *this;
	}
	//  Copy constructor
	shared_ptr_toy(const shared_ptr_toy& oth_toy) {
		toy = oth_toy.toy;
		cb = oth_toy.cb;
		cb->increase_counter();
		std::cout << "counter increased: " << cb->get_counter() << std::endl;
	}
	// Destructor
	~shared_ptr_toy() {
		cb->decrease_counter();
		std::cout << "counter decreased: " << cb->get_counter() << std::endl;
		if (cb->get_counter() == 0) {
			delete toy;
			std::cout << "toy deleted\n";
			delete cb;
			std::cout << "control block deleted\n";
		}
	}
	Toy* get_shared_toy() const {
		return toy;
	}
	ControlBlock* get_shared_cb() const {
		return cb;
	}
	void set_shared_toy(Toy* new_toy) {
		toy = new_toy;
	}
	void set_shared_cb(ControlBlock* new_cb) {
		cb = new_cb;
	}
};
// Make shared from name
shared_ptr_toy make_shared(const std::string& name) {
	shared_ptr_toy* new_toy = new shared_ptr_toy();
	Toy* temp_toy = new Toy(name);
	ControlBlock* temp_cb = new ControlBlock();
	new_toy->set_shared_toy(temp_toy);
	new_toy->set_shared_cb(temp_cb);
	std::cout << name << " shared_ptr_toy from name created\n";
	return* new_toy;
	delete temp_toy;
	delete temp_cb;
}
 //Make shared from another toy
shared_ptr_toy make_shared(const shared_ptr_toy& oth_toy) {
	shared_ptr_toy* new_toy = new shared_ptr_toy();
	Toy* temp_toy = oth_toy.get_shared_toy();
	ControlBlock* temp_cb = oth_toy.get_shared_cb();
	new_toy->set_shared_toy(temp_toy);
	new_toy->set_shared_cb(temp_cb);
	std::cout << "shared_ptr_toy from another toy created\n";
	std::cout << "counter increased: " <<
		new_toy->get_shared_cb()->get_counter() << std::endl;
	return *new_toy;
	delete temp_toy;
	delete temp_cb;
}

int main()
{
	// Toy constructor 
	Toy* toy = new Toy("Default");
	//Make shared from name
	shared_ptr_toy toy1;
	toy1 = make_shared("Doll");
	// Make shared from another toy
	shared_ptr_toy toy2;
	toy2 = make_shared(toy1);
	delete toy;
}


